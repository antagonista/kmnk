﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMNK_GUI
{
    public partial class Form1 : Form
    {

        public List<Zmienna> zmienneDane; 



        private void OdswiezOkno()
        {

            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.Items.Clear();
            checkedListBox1.Items.Clear();

            foreach (Zmienna rekord in zmienneDane)
            {

                comboBox1.Items.Add(rekord.nazwaZmiennej);
                checkedListBox1.Items.Add(rekord.nazwaZmiennej);
            }

            if (comboBox1.Items.Count != 0) comboBox1.SelectedIndex = 0; 
            

            //comboBox1.GetItemText(comboBox1.SelectedItem);

            


        }
        
        private void Deserializacja()
        {

            try
            {
                using (Stream stream = File.Open("dane.bin", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    zmienneDane = (List<Zmienna>)bin.Deserialize(stream);
                    
                }
            }
            catch (IOException)
            {

                zmienneDane = new List<Zmienna>();

            }


            //Matrix matrix1 = new Matrix(5, 10, 15, 20, 25, 30);

            //MessageBox.Show(String.Format("x - {0}; y - {1}", matrix1.OffsetX, matrix1.OffsetY));


        }

        private void Serializacja()
        {

            try
            {
                using (Stream stream = File.Open("dane.bin", FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, zmienneDane);
                }
            }
            catch (IOException)
            {
            }


        }
        
        
        public Form1()
        {
            InitializeComponent();

            Deserializacja();

            OdswiezOkno();

            

        }

        private void button3_Click(object sender, EventArgs e)
        {

            ZarzadzanieZmiennymi zarzadzanieZmiennymi = new ZarzadzanieZmiennymi(this);
            zarzadzanieZmiennymi.Owner = this;
            zarzadzanieZmiennymi.ShowDialog();

            OdswiezOkno();


        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (checkedListBox1.SelectedItems.Count != 0)
            {
                AkcjeModelu oszacujModel = new AkcjeModelu(this);
                oszacujModel.Owner = this;

                if (oszacujModel.Created == true)
                {
                    oszacujModel.Show();
                }
            }
            else MessageBox.Show("Musisz wybrać zmienne objaśniające!");

            

            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Serializacja();
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {





        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
