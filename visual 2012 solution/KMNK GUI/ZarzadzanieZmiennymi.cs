﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMNK_GUI
{
    public partial class ZarzadzanieZmiennymi : Form
    {

        Form1 oknoGlowne;
        
        public ZarzadzanieZmiennymi(Form1 form1)
        {

            this.oknoGlowne = form1;
            
            InitializeComponent();

            OdswiezUI();
        }

        private string FormatowanieZmiennej(String zmienna)
        {
            string[] element = zmienna.Split(zmienna[0]);

            return element[1];
        }


        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog plikWybrany = new OpenFileDialog();
            plikWybrany.Filter = "Plik *.csv|*.csv";


            if (plikWybrany.ShowDialog() == DialogResult.OK)
            {

                string dirPliku = plikWybrany.FileName.ToString();

                string[] plikCsv = System.IO.File.ReadAllText(@dirPliku).Split(';');

                string nazwaZmiennej = FormatowanieZmiennej(plikCsv[7]);
                string agregatZmiennej = FormatowanieZmiennej(plikCsv[12]);

                List<double> tablicaDanych = new List<double>();
                List<int> tablicaLat = new List<int>();

                int licznikPowtorzenAgregatu = 1;

                // sprawdzanie ilosci okresow (po powtarzajacych sie agregatach)
                for (int x = 13; x < plikCsv.Length - 1; x++)
                {

                    if (plikCsv[x - 1] == plikCsv[x]) licznikPowtorzenAgregatu++;
                    else break;


                    //Console.WriteLine("[{0}] - {1}", x, plikCsv[x]);

                    //if (plikCsv[x] == "\n") Console.Write(" znak nowej lini \n\n");
                    //if (plikCsv[x] == "") Console.Write(" puste pole :) \n\n");
                }

                int licznikPowtorzenLat = 0;

                DateTime now = DateTime.Now;
                int rokAktualny = Convert.ToInt16(now.ToString("yyyy"));


                // szukanie elementow, ktore odpowiadaja za konkretne lata -> po formacie odpowiednim dla lat "LLLL"
                int xTemp = 12 + licznikPowtorzenAgregatu;
                for (int x = xTemp; x < plikCsv.Length - 1; x++)
                {

                    if (licznikPowtorzenLat == licznikPowtorzenAgregatu) break;

                    if (plikCsv[x].Length == 6)
                    {
                        // obcinanie elementu o " "
                        string ciagElement = String.Format("{0}{1}{2}{3}", plikCsv[x][1], plikCsv[x][2], plikCsv[x][3], plikCsv[x][4]);

                        // sprawdzanie czy ciag tworzy rok
                        if (Regex.IsMatch(ciagElement, @"[0-9]{4}") == true)
                        {
                            //Console.Write("{0}\n", plikCsv[x]);

                            int zmiennaLat = Convert.ToInt16(ciagElement);

                            // wartosc miesci sie w granicach dopuszczalnych dla lat - dodanie do tablicy
                            if (zmiennaLat >= 1980 && zmiennaLat <= rokAktualny)
                            {
                                tablicaLat.Add(zmiennaLat);
                                //Console.Write("{0}\n", zmiennaLat);

                                licznikPowtorzenLat++;
                            }

                        }

                    }
                    else continue;

                }

                // szukanie wartosci zmiennej i dodawanie ich do listy

                int momentRozpoczeciaIteracji = plikCsv.Length - (licznikPowtorzenLat + 1);

                for (int x = momentRozpoczeciaIteracji; x + 1 < plikCsv.Length; x++)
                {

                    //string[] element = plikCsv[x].Split(plikCsv[x][0]);

                    //Console.WriteLine("[{1}] wartosc zmiennej {0} {2}", plikCsv[x], x, element[1]);

                    //tablicaDanych.Add(Convert.ToDouble(element[1]));

                    tablicaDanych.Add(Convert.ToDouble(FormatowanieZmiennej(plikCsv[x])));

                }

                

                bool jestZmienna = false;

                foreach (Zmienna rekord in oknoGlowne.zmienneDane)
                {
                    if (rekord.nazwaZmiennej == nazwaZmiennej)
                    {
                        jestZmienna = true;
                        break;
                    }
                        
                }

                if (jestZmienna == false)
                {
                    MessageBox.Show("Dodano nowa zmienna");

                    /*
                    string temp = "";

                    foreach (double dana in tablicaDanych)
                    {
                        temp = temp + dana + "\n";
                    }

                    MessageBox.Show(temp);
                    */
                    oknoGlowne.zmienneDane.Add(new Zmienna(String.Format("{0} ({1})",nazwaZmiennej,agregatZmiennej), agregatZmiennej, tablicaDanych, tablicaLat));
                }
                else
                {
                    MessageBox.Show("Istnieje juz zmienna o tej nazwie!");
                }

                
                
                

                /*
                Console.WriteLine("Nazwa zmiennej: {0} ({1}) - dla lat {2} - {3}", nazwaZmiennej, agregatZmiennej, tablicaLat[0], tablicaLat[tablicaLat.Count - 1]);

                for (int x = 0; x < licznikPowtorzenLat; x++)
                {
                    Console.WriteLine("{0} -> {1}", tablicaLat[x], tablicaDanych[x]);
                }
                 * 
                */

            }
            else
            {
                Close();
            }



            OdswiezUI();



        }

        private void OdswiezUI ()
        {

            checkedListBox1.Items.Clear();

            foreach (Zmienna rekord in oknoGlowne.zmienneDane)
            {
                string temp = String.Format("{0}", rekord.nazwaZmiennej);
                checkedListBox1.Items.Add(temp);
            }


        }



        private void button3_Click(object sender, EventArgs e)
        {
            // zamknij program           
            Close();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            // usuwanie zmiennej

            string msn = String.Format("Na pewno usunac zmienna:\n\n{0}\n\n?", checkedListBox1.GetItemText(checkedListBox1.SelectedItem));
                
                

            //checkedListBox1.GetItemText(checkedListBox1.SelectedItem);


            //msn = msn + checkedListBox1.CheckedItems.ToString();

            if (MessageBox.Show(msn) == DialogResult.OK)
            {
                
                //oknoGlowne.zmienneDane.re
                foreach (Zmienna rekord in oknoGlowne.zmienneDane)
                {
                    if (String.Format("{0} ({1})", rekord.nazwaZmiennej, rekord.nazwaAgregatu) == checkedListBox1.GetItemText(checkedListBox1.SelectedItem))
                    {                       
                        oknoGlowne.zmienneDane.Remove(rekord);
                        break;
                    }
                }

                OdswiezUI();
            }


        }
    }
}
