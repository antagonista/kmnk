﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMNK_GUI
{
    public partial class TestHartleya : Form
    {

        public AkcjeModelu OknoGlowne;
        
        public TestHartleya(AkcjeModelu okno, int stopnieSwobody, int parametry)
        {

            OknoGlowne = okno;   

            InitializeComponent();

            label2.Text = String.Format("Dla: alfa = 0.05; k={0}; n-1 (n_r) = {1}", parametry, stopnieSwobody);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "")
            {

                try
                {
                    OknoGlowne.statystykaH = double.Parse(textBox1.Text);
                    Close();

                }
                catch
                {
                    MessageBox.Show("Możesz jedynie podać wartości zmiennoprzecinkowe");
                }


            }
            else
            {
                MessageBox.Show("Musisz podać wartość!");
            }



        }
    }
}
