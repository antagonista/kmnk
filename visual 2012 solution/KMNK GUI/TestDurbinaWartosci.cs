﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMNK_GUI
{
    public partial class TestDurbinaWartosci : Form
    {

        public AkcjeModelu Glowne;
        
        public TestDurbinaWartosci(AkcjeModelu dostep, int liczbaObserwacji, int liczbaParametrow)
        {
             Glowne= dostep;

            
             InitializeComponent();

             label1.Text = String.Format("Podaj wartości dl i du dla alfa = 0.05, n = {0}, k = {1}", liczbaObserwacji, liczbaParametrow); 


        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" && textBox2.Text != "")
            {

                try
                {

                    Glowne.wartoscDL = Double.Parse(textBox1.Text);
                    Glowne.wartoscDU = Double.Parse(textBox2.Text);

                    Close();
                }
                catch
                {
                    MessageBox.Show("Możesz podać jedynie wartości liczbowe (zmiennoprzecinkowe)");

                }

            }
            else
            {
                MessageBox.Show("Musisz podać wartości!");
            }




        }
    }
}
