﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMNK_GUI
{
    [Serializable()]
    public class Zmienna
    {

        public string nazwaZmiennej;
        public string nazwaAgregatu;

        public List<double> tablicaDanych = new List<double>();
        public List<int> tablicaLat = new List<int>();




        public Zmienna(string nazwa, string agregat, List<double> dane, List<int> lata)
        {

            this.nazwaZmiennej = nazwa;
            this.nazwaAgregatu = agregat;
            this.tablicaDanych = dane;
            this.tablicaLat = lata;

        }




    }
}
