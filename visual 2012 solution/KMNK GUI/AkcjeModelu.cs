﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DotNetMatrix;
using ZedGraph;

namespace KMNK_GUI
{
    public partial class AkcjeModelu : Form
    {

        Form1 OknoGlowne;

        string zmiennaY;
        List<string> zmienneX = new List<string>();

        //double[] macierzY = new double[];

        private int liczbaObserwacji = 0;
        private int liczbaSzacowanychParametrow = 0; // juz z parametrem wolnym uwzglednione
        private int okresPoczatkowy = 0;
        private int okresKoncowy = 0;

        private double[] macierzY;
        private double[,] macierzX;
        private double[] alfy;
        private double[] Yteoretyczny;
        private double[] odchyleniaResztowe;
        private double sredniaY;
        private double[] odchyleniaResztoweKwadrat;
        private double sumaOdchylenieResztoweKwadrat;
        private double sumaRoznicaYsredniaY;
        private double sumaEtEtkwadrat;

        private double wspolczynnikR;
        private double wspolczynnikFi;
        private double liczbaStopniSwobody;
        private double wspolczynnikSKwadratE;
        private double wspolczynnikSe;
        private double wspolczynnikVse;

        private double[] MacierzOdwrotnaXtX;
        private double[] WektorDAlfTeoretycznych;
        private double[] wektorTstudenta;

        private double statystykaD;

        public double wartoscDL;
        public double wartoscDU;
        private bool wspolczynnikPTDW = false; // true - d >=2; false d < 2
        private string wynikTestuDW;
        private string intTestuDW;
        private int najdluzszaSeriaK;
        private int wartoscKtestu;
        private string wynikTestuSerii;
        private string intTestuSerii;
        private double statystykaF;
        public double statystykaH;

        private bool wykonanoTestHartleya = false;
        private string wynikTestuH;
        private string intTestuH;
        
        
       


        private void SprawdzenieZmiennych()
        {

            string temp = OknoGlowne.comboBox1.GetItemText(OknoGlowne.comboBox1.SelectedItem);
            string[] tempN = temp.Split(' ');

            //zmiennaY = tempN[0];
            //MessageBox.Show(zmiennaY);
            zmiennaY = OknoGlowne.comboBox1.GetItemText(OknoGlowne.comboBox1.SelectedItem);

            foreach (string rekord in OknoGlowne.checkedListBox1.CheckedItems)
            {
                //tempN = rekord.Split(' ');
                zmienneX.Add(rekord.ToString());
                listBZO.Items.Add(rekord.ToString());
            }

            // sprawdzenie czy zmienna Y nie pokrywa sie z X'owa
            foreach (string rekord in zmienneX)
            {
                if (rekord == zmiennaY)
                {
                           
                    MessageBox.Show("Nie mozesz wybrac takiej samej zmiennej Y i X !");

                    Close();

                }
            }

            lZBText.Text = zmiennaY;

            // sprawdzanie czy jest odpowiednia ilosc danych 

            // ustalenie max i min okresow dla zmiennej Y

            int minLatTemp = 0;
            int maxLatTemp = 0;
            


            foreach (Zmienna rekord in OknoGlowne.zmienneDane)
            {

                if (rekord.nazwaZmiennej == zmiennaY)
                {
                    maxLatTemp = rekord.tablicaLat.Max();
                    minLatTemp = rekord.tablicaLat.Min();
                    break;
                }

            }

            // sprawdzenie czy jest odpowiednia ilosc danych X

            foreach (string nazwyX in zmienneX)
            {

                foreach (Zmienna rekord in OknoGlowne.zmienneDane)
                {

                    if (rekord.nazwaZmiennej == nazwyX)
                    {

                        if (maxLatTemp >= rekord.tablicaLat.Max()) maxLatTemp = rekord.tablicaLat.Max();

                        if (minLatTemp <= rekord.tablicaLat.Min()) minLatTemp = rekord.tablicaLat.Min();


                        break;
                    }


                }


            }

            //string msn = String.Format("mozliwy przedzial lat {0} - {1}", minLatTemp, maxLatTemp);
            //MessageBox.Show(msn);


            liczbaObserwacji = maxLatTemp - minLatTemp;
            okresKoncowy = maxLatTemp;
            okresPoczatkowy = minLatTemp;
            liczbaSzacowanychParametrow = zmienneX.Count + 1;

            if (liczbaSzacowanychParametrow > liczbaObserwacji)
            {
                MessageBox.Show("Niestety nie można oszacować modelu!\n\nLiczba szacowanych parametrow jest wieksza od liczby obserwacji!");
                Close();
            }
            
        }

        private void PrzypisywanieWartosciDoMacierzy()
        {

            // wczytywanie wartosci do macierzy Y

            macierzY = new double[liczbaObserwacji];

            foreach (Zmienna rekord in OknoGlowne.zmienneDane)
            {
                if (zmiennaY == rekord.nazwaZmiennej)
                {

                    int indexPoczatek = 0;
                    int indexKoniec = 0;
                    int licznik = 0;
                    
                    foreach (int index in rekord.tablicaLat)
                    {

                        if (index == okresPoczatkowy) indexPoczatek = licznik;

                        if (index == okresKoncowy) indexKoniec = licznik;

                        licznik++;

                    }

                    licznik = 0;
                    int licznikMacierzy = 0;

                    // przypisywanie danych wlasciwych

                    foreach (double lata in rekord.tablicaDanych)
                    {

                        if (licznik >= indexKoniec) break;


                        if (licznik >= indexPoczatek)
                        {
                            macierzY[licznikMacierzy] = lata;
                            licznikMacierzy++;
                        }


                        licznik++;
                    }



                    break;
                }
            }


            // uzupelnianie danych macierzy X

            macierzX = new double[liczbaObserwacji,liczbaSzacowanychParametrow]; //liczbaSzacowanychParametrow

            // uzupelnienie pierwszej kolumny jedynkami

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                macierzX[x,0] = 1;
            }

            // uzupelnienie reszty macierzy - o dane dla zmiennych objasniajacych

            for (int x = 0; x < liczbaSzacowanychParametrow - 1; x++)
            {

                // zmienneX[x] - obecnie katowana zmienna (string)

                foreach (Zmienna rekord in OknoGlowne.zmienneDane)
                {

                    if (zmienneX[x] == rekord.nazwaZmiennej)
                    {

                        int indexPoczatek = 0;
                        int indexKoniec = 0;
                        int licznik = 0;

                        foreach (int index in rekord.tablicaLat)
                        {

                            if (index == okresPoczatkowy) indexPoczatek = licznik;

                            if (index == okresKoncowy) indexKoniec = licznik;

                            licznik++;

                        }

                        licznik = 0;
                        int licznikMacierzy = 0;

                        // przypisywanie danych wlasciwych

                        foreach (double lata in rekord.tablicaDanych)
                        {

                            if (licznik >= indexKoniec) break;


                            if (licznik >= indexPoczatek)
                            {
                                macierzX[licznikMacierzy,x+1] = lata;
                                licznikMacierzy++;
                            }


                            licznik++;
                        }

                        break;
                    }



                }




            }


            /*

             * 
             * sprawdzenie jak wygladaja macierze
             * 
            string msn = String.Format("Ilosc Obserwacji: {0}\n\n",liczbaObserwacji);

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                msn = msn + String.Format("{0}\n", macierzY[x]);
            }

            MessageBox.Show(msn);

            msn = "Macierz X wyglada nastepujaco: \n\n";

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                for (int y = 0; y < liczbaSzacowanychParametrow; y++)
                {
                    msn = msn + macierzX[x, y].ToString() + " ";

                    if (y == liczbaSzacowanychParametrow-1) msn = msn + " \n";
                }
            }
            
            MessageBox.Show(msn);
            */






        }

        private void KMNK()
        {

            // rzutowanie nn wymagania GeneralMatrix

            double[][] nowaY = new double[liczbaObserwacji][];
            double[][] nowaX = new double[liczbaObserwacji][];

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                //nowaY
                nowaY[x] = new double[1];
                nowaY[x][0] = macierzY[x];

                // nowa X
                nowaX[x] = new double[liczbaSzacowanychParametrow];

                for (int y = 0; y < liczbaSzacowanychParametrow; y++)
                {
                    nowaX[x][y] = macierzX[x,y];
                }


            }

            GeneralMatrix Y = new GeneralMatrix(nowaY);
            GeneralMatrix X = new GeneralMatrix(nowaX);

            GeneralMatrix Xtransponowane = X.Transpose();

            GeneralMatrix XtX = Xtransponowane.Multiply(X);

            GeneralMatrix XtXOdw = XtX.Inverse();

            MacierzOdwrotnaXtX = new double[liczbaSzacowanychParametrow];

            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                MacierzOdwrotnaXtX[x] = XtXOdw.GetElement(x,x);
            }

            GeneralMatrix XtY = Xtransponowane.Multiply(Y);

            GeneralMatrix alfyDone = XtXOdw.Multiply(XtY);

            GeneralMatrix YteoretyczneTemp = X.Multiply(alfyDone);



            alfy = new double[liczbaSzacowanychParametrow];

            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                textBox1.AppendText(String.Format("alfa{0}: {1}\r\n", x, alfyDone.GetElement(x, 0)));
                alfy[x] = alfyDone.GetElement(x, 0);
            }

            Yteoretyczny = new double[liczbaObserwacji];

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                Yteoretyczny[x] = YteoretyczneTemp.GetElement(x, 0);
            }



        }

        private void MiaryDopasowania()
        {

            // liczenie roznicy odchylen resztowych

            odchyleniaResztowe = new double[liczbaObserwacji];

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                odchyleniaResztowe[x] = macierzY[x] - Yteoretyczny[x]; 
            }

            // liczenie sredniej dla obserwacji Y

            double sredniaTemp = 0;

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                sredniaTemp = sredniaTemp + macierzY[x];
            }

            sredniaY = sredniaTemp / liczbaObserwacji;
            //MessageBox.Show(String.Format("srednia Y: {0}", sredniaY));
            // liczenie kwadratow odchylen resztowych

            odchyleniaResztoweKwadrat = new double[liczbaObserwacji];

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                odchyleniaResztoweKwadrat[x] = odchyleniaResztowe[x] * odchyleniaResztowe[x];
            }

            double sumaTemp = 0;

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                sumaTemp = sumaTemp + odchyleniaResztoweKwadrat[x];
            }

            sumaOdchylenieResztoweKwadrat = sumaTemp;
            //MessageBox.Show(String.Format("suma et: {0}", sumaOdchylenieResztoweKwadrat));

            // liczenie roznicy Yt - srednia Yt podniesionej do kwadratu oraz sumy

            double[] roznicaYsredniaY = new double[liczbaObserwacji];

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                roznicaYsredniaY[x] = Math.Pow(macierzY[x] - sredniaY,2);
            }

            sumaTemp = 0;

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                sumaTemp = sumaTemp + roznicaYsredniaY[x];
            }

            sumaRoznicaYsredniaY = sumaTemp;

            // liczenie (et-et_-1)^2

            double[] roznicaEtEt1kwadrat = new double[liczbaObserwacji-1];

            for (int x = 1; x < liczbaObserwacji-1; x++)
            {
                roznicaEtEt1kwadrat[x] = Math.Pow(odchyleniaResztowe[x] - odchyleniaResztowe[x - 1], 2);
            }

            sumaTemp = 0;

            for (int x = 1; x < liczbaObserwacji - 1; x++)
            {
                sumaTemp = sumaTemp + roznicaEtEt1kwadrat[x];
            }

            sumaEtEtkwadrat = sumaTemp;

            //MessageBox.Show(String.Format("suma et^2: {0}", sumaEtEtkwadrat));
            // obliczenia wspolczynnikow miar dopasowania

            wspolczynnikR = 1-(sumaEtEtkwadrat/sumaRoznicaYsredniaY);
            wspolczynnikFi = 1 - wspolczynnikR;

            liczbaStopniSwobody = liczbaObserwacji - liczbaSzacowanychParametrow;

            wspolczynnikSKwadratE = liczbaStopniSwobody / sumaOdchylenieResztoweKwadrat;

            wspolczynnikSe = Math.Sqrt(wspolczynnikSKwadratE);

            wspolczynnikVse = wspolczynnikSe / sredniaY;

            // umieszczenie wartosci w formie

            label6.Text = String.Format("fi^2: {0}%", wspolczynnikFi*100);
            label7.Text = String.Format("R^2: {0}%", wspolczynnikR*100);
            label9.Text = String.Format("Se^2: {0}", wspolczynnikSKwadratE);
            label8.Text = String.Format("Se: {0}", wspolczynnikSe);
            label12.Text = String.Format("Vs: {0}%", wspolczynnikVse*100);


        }

        private void IstotnoscParametrowStrukturalnych()
        {

            WektorDAlfTeoretycznych = new double[liczbaSzacowanychParametrow];
            wektorTstudenta = new double[liczbaSzacowanychParametrow];

            string testText = "";

            // wyciagniecie wartosci dla alfa 0 z macierzy XtX odwrotne oraz obliczenia pomocnicze do t-studenta

            WektorDAlfTeoretycznych[0] = Math.Sqrt(wspolczynnikSKwadratE * MacierzOdwrotnaXtX[liczbaSzacowanychParametrow-1]);

            //testText += String.Format("{0}\n",WektorDAlfTeoretycznych[0]);

            for (int x = 1; x < liczbaSzacowanychParametrow; x++)
            {

                WektorDAlfTeoretycznych[x] = Math.Sqrt(wspolczynnikSKwadratE * MacierzOdwrotnaXtX[x-1]);
                //testText += String.Format("{0}\n", WektorDAlfTeoretycznych[x]);
            }

            //MessageBox.Show(testText);

            // obliczenie wartosci t-studenta

            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                //WektorDAlfTeoretycznych[x] = Math.Sqrt(wspolczynnikSKwadratE * MacierzOdwrotnaXtX[x]);

                wektorTstudenta[x] = Math.Abs(alfy[x])/WektorDAlfTeoretycznych[x];

                string msn = "";

                if (wektorTstudenta[x] > 2)
                {
                    msn = String.Format("t_alfa{0}: {1} > 2\r\n", x, wektorTstudenta[x]);
                }
                else
                {
                    msn = String.Format("t_alfa{0}: {1} < 2\r\n", x, wektorTstudenta[x]);
                }

                textBox2.AppendText(msn);
            }
            



        }

        private void BadanieWlasnosciSkladnikaLosowego()
        {

            // test Durbina-Watsona

            statystykaD = sumaEtEtkwadrat / sumaRoznicaYsredniaY;

            // statystyka d >= 0
            if (statystykaD >= 2)
            {
                
                label1.Text = "H1: p < 0 (dodatnia autokorelacja)";
                // odliczanie statystyki d`

                statystykaD = 4 - statystykaD;

            }
            else
            {
                wspolczynnikPTDW = true;
                label1.Text = "H1: p > 0 (ujemna autokorelacja)";
            }

            // okno dialogowe z podaniem wartosci z tablic

            TestDurbinaWartosci WartosciDW = new TestDurbinaWartosci(this,liczbaObserwacji,liczbaSzacowanychParametrow-1);
            WartosciDW.ShowDialog();

            //MessageBox.Show(String.Format("{0} {1}", wartoscDL, wartoscDU));

            if (statystykaD <= wartoscDL)
            {
                wynikTestuDW = "Wynik testu: d <= dL";
                label21.Text = "Wynik testu: d <= dL";
                intTestuDW = "Int: H0 nalezy odrzucic (jest autokorelacja)";
                label22.Text = "Int: H0 należy odrzucić (jest autokorelacja)";
            }
            else if (statystykaD >= wartoscDU)
            {
                wynikTestuDW = "Wynik testu: d >= dU";
                intTestuDW = "Int: brak podstaw do odrzucenia H0 (brak autokorelacji)";
                label21.Text = "Wynik testu: d >= dU";
                label22.Text = "Int: brak podstaw do odrzucenia H0 (brak autokorelacji)";
            }
            else
                // 
            {
                label21.Text = "Wynik testu: dL < d < dU";
                wynikTestuDW = "Wynik testu: dL < d < dU";
                // obliczanie wspolczynnika autokorelacji
                double wspolczynnikAutokorelacjiP = (2 - statystykaD) / 2;

                if (wspolczynnikAutokorelacjiP >= 0.6)
                {
                    label22.Text = "Int: p^ >= 0.6 (istotna autokorelacja)";
                    intTestuDW= "Int: p^ >= 0.6 (istotna autokorelacja)";
                }
                else
                {
                    label22.Text = "Int: p^ < 0.6 (brak autokorelacji)";
                    intTestuDW = "Int: p^ < 0.6 (brak autokorelacji)";
                }

            }



            // test serii

            // wyznaczenie nadluzszej serii

            List<int> liczebnosciSerii = new List<int>();

            int najdluzszaA = 0;
            int najdluzszaB = 0;
            int tempA = 0; // et > 0
            int tempB = 0; // et < 0
            bool seriaA = false;
            bool seriaB = false;

            string testText = "";

            for (int x = 0; x < liczbaObserwacji; x++)
            {

                if (odchyleniaResztowe[x] > 0)
                {

                    if (seriaB == true)
                    {
                        seriaB = false;
                        liczebnosciSerii.Add(tempB);
                        if (tempB > najdluzszaB) najdluzszaB = tempB;
                        tempB = 0;
                    }

                    testText += " A ";

                    seriaA = true;
                    tempA++;

                }
                else
                {
                    if (seriaA == true)
                    {
                        seriaA = false;
                        liczebnosciSerii.Add(tempA);
                        if (tempA > najdluzszaA) najdluzszaA = tempA;
                        tempA = 0;
                    }

                    testText += " B ";

                    seriaB = true;
                    tempB++;
                }


                if (x == (liczbaObserwacji - 1))
                    // ostatnia petla
                {

                    if (seriaA == true) liczebnosciSerii.Add(tempA);
                    else liczebnosciSerii.Add(tempB);

                    if (seriaA == true && tempA > najdluzszaA)
                    {
                        najdluzszaA = tempA;
                    }
                    else if (seriaB == true && tempB > najdluzszaB)
                    {
                        najdluzszaB = tempB;
                    }


                }




            }


            if (najdluzszaA > najdluzszaB) najdluzszaSeriaK = najdluzszaA;
            else najdluzszaSeriaK = najdluzszaB;

            //MessageBox.Show(String.Format("serie: {0} \n\nnajdluzsza seria: {1} {2}", testText, najdluzszaA, najdluzszaB));

            if (liczbaObserwacji <= 10) wartoscKtestu = 5;
            else if (liczbaObserwacji > 10 && liczbaObserwacji <= 14) wartoscKtestu = 6;
            else if (liczbaObserwacji > 14 && liczbaObserwacji <= 22) wartoscKtestu = 7;
            else if (liczbaObserwacji > 22 && liczbaObserwacji <= 34) wartoscKtestu = 8;
            else if (liczbaObserwacji > 34 && liczbaObserwacji <= 54) wartoscKtestu = 9;
            else if (liczbaObserwacji > 54) wartoscKtestu = 10;

            if (najdluzszaSeriaK >= wartoscKtestu)
            {
                wynikTestuSerii = "Wynik testu: K >= K*";
                intTestuSerii = "Int: odrzucamy H0 (reszty nie losowe)";
                label23.Text = "Int: odrzucamy H0 (reszty nie losowe)";
                label24.Text = "Wynik testu: K >= K*";
            }
            else
            {
                label23.Text = "Int: brak podstaw do odrzucenia H0 (losowe reszty)";
                label24.Text = "Wynik testu: K < K*";

                wynikTestuSerii = "Wynik testu: K < K*";
                intTestuSerii = "Int: brak podstaw do odrzucenia H0 (losowe reszty)";
            }




            // test hartleya

            // dla malej liczby obserwacji

            int liczebnoscPierwszejSerii = (int)Math.Ceiling((double)(liczbaObserwacji / 2))+1;
            int liczebnoscDrugiejSerii = liczbaObserwacji - liczebnoscPierwszejSerii;

            if (liczebnoscPierwszejSerii >= 5 && liczebnoscDrugiejSerii >= 5)
            {
                wykonanoTestHartleya = true;
                double tempSuma = 0;
                double tempSrednia = 0;

                List<double> TempWariancje = new List<double>();
                List<double> WariancjeMaxMin = new List<double>();

                //MessageBox.Show(String.Format("{0} {1}", liczebnoscPierwszejSerii, liczebnoscDrugiejSerii));

                for (int x = 0; x < liczbaObserwacji; x++)
                {

                    if (x < liczebnoscPierwszejSerii)
                    {

                        tempSuma = tempSuma + odchyleniaResztowe[x];
                        TempWariancje.Add(odchyleniaResztowe[x]);

                        if (x == liczebnoscPierwszejSerii - 1)
                        // ostatnie
                        {
                            tempSrednia = tempSuma / liczebnoscPierwszejSerii;
                            tempSuma = 0;

                            for (int y = 0; y < TempWariancje.Count; y++)
                            {
                                tempSuma = tempSuma + Math.Pow((TempWariancje[y] - tempSrednia), 2);
                            }


                            double wartosc = tempSuma / (liczebnoscPierwszejSerii - liczbaSzacowanychParametrow);

                            WariancjeMaxMin.Add(wartosc);

                            tempSuma = 0;
                            TempWariancje.Clear();
                            tempSrednia = 0;

                        }


                    }
                    else
                    {

                        tempSuma = tempSuma + odchyleniaResztowe[x];
                        TempWariancje.Add(odchyleniaResztowe[x]);

                        if (x == liczbaObserwacji - 1)
                        // ostatnie
                        {
                            tempSrednia = tempSuma / liczebnoscPierwszejSerii;
                            tempSuma = 0;

                            for (int y = 0; y < TempWariancje.Count; y++)
                            {
                                tempSuma = tempSuma + Math.Pow((TempWariancje[y] - tempSrednia), 2);
                            }


                            double wartosc = tempSuma / (liczebnoscPierwszejSerii - liczbaSzacowanychParametrow);

                            WariancjeMaxMin.Add(wartosc);

                            tempSuma = 0;
                            TempWariancje.Clear();
                            tempSrednia = 0;

                        }



                    }



                }

                TestHartleya OknoNowe = new TestHartleya(this, (liczebnoscPierwszejSerii - liczbaSzacowanychParametrow), liczbaSzacowanychParametrow-1);
                OknoNowe.ShowDialog();



                statystykaF = WariancjeMaxMin.Max() / WariancjeMaxMin.Min();

                if (statystykaF <= statystykaH)
                {
                    wynikTestuH = "Wynik testu: F < H*";
                    intTestuH = "Int: wariancja jest stała";
                    
                    label26.Text = "Wynik testu: F < H*";
                    label25.Text = "Int: wariancja jest stała";
                }
                else
                {
                    label26.Text = "Wynik testu: F > H*";
                    label25.Text = "Int: wariancja nie jest stała";

                    wynikTestuH = "Wynik testu: F > H*";
                    intTestuH = "Int: wariancja nie jest stała";
                }

            }
            else
            {
                MessageBox.Show("Za mała liczba obserwacji, aby wykonać test Hartley'a!");
                label26.Text = "Nie można wykonać testu";
                label25.Text = "";
            }



            


            // H* = 7.15 - można używać ogólnie dla małej liczby obserwacji








            /* dla wiekszej liczby oserwacji - nie ma sensu uzywac przy danych z gus, gdzie jest max 12 obserwacji dla przedzialow czasowych

            List<double> wartosciWariancji = new List<double>();
            List<double> tempWartosci = new List<double>();

            double tempSuma = 0;
            double tempSrednia = 0;
            int tempIleSeria = 0;



            
            seriaA = false;
            seriaB = false;

            int tempLicznik = 0;


            for (int x = 0; x < liczebnosciSerii.Count; x++)
            {

                tempWartosci.Clear();
                tempSuma = 0;
                tempSrednia = 0;

                for (int y = 0; y < liczebnosciSerii[x]; y++)
                {

                    tempWartosci.Add(odchyleniaResztowe[tempLicznik]);
                    tempSuma = tempSuma + odchyleniaResztowe[tempLicznik];

                    tempLicznik++;

                }

                tempSrednia = tempSuma / liczebnosciSerii[x];

                for (int y = 0; y < liczebnosciSerii[x]; y++)
                {
                    tempSuma = tempSuma + Math.Pow((tempWartosci[x] - tempSrednia), 2);
                }

                double tempWartosc = tempSuma / (liczebnosciSerii[x] - liczbaSzacowanychParametrow);

                wartosciWariancji.Add(tempWartosc);


            }


            */




            








        }

        private void TworzenieWykresow(string nazwaZmiennej)
        {

            

            double[] osY = new double[liczbaObserwacji];

            //string test = "";

            foreach (Zmienna rekord in OknoGlowne.zmienneDane)
            {
                if (rekord.nazwaZmiennej == nazwaZmiennej)
                {
                    int licznik = 0;
                    foreach (int lata in rekord.tablicaLat)
                    {
                        if (lata == okresPoczatkowy) break;
                        licznik++;
                    }

                    for (int x = 0; x < liczbaObserwacji; x++)
                    {

                            osY[x] = rekord.tablicaDanych[licznik+x];
                            //test = test + rekord.tablicaDanych[x] + "\n";

                    }

                    break;
                }


            }

            //MessageBox.Show(test);

            double[] osX = new double[liczbaObserwacji];
            

            // przetwarzenie danych na potrzeby wykresu

            for (int x = 0; x < liczbaObserwacji; x++)
            {
                osX[x] = okresPoczatkowy + x;
            }



            // tworzenie wykresu

            wykres.GraphPane.CurveList.Clear();

            // GraphPane object holds one or more Curve objects (or plots)
            GraphPane myPane = wykres.GraphPane;
            myPane.XAxis.Title.Text = "Lata";
            myPane.YAxis.Title.Text = "Wartości";
            myPane.Title.Text = "Graficzne przedstawienie danych";

            PointPairList spl1 = new PointPairList(osX, osY);

            LineItem linia = myPane.AddCurve(nazwaZmiennej, spl1, Color.Blue, SymbolType.None);
            

            wykres.AxisChange();
            wykres.Invalidate();
            wykres.Refresh();


        }

        private void GenerujRaport(string dirZapisu)
        {

            
            StreamReader objReader = new StreamReader("szablon.html");
            string szablon = objReader.ReadToEnd();
            objReader.Close();

            // uzupelnienie tabeli z danymi

            string temp = "<table border=1>";

            temp = temp + "<tr><td>lata</td><td>Y</td>";

            // tworzenie naglowka tabeli

            for (int x = 1; x <= liczbaSzacowanychParametrow-1;x++)
            {
                temp = temp + "<td>X"+x+"</td>";
            }
            
            temp = temp + "</tr>";

            // wypelnianie danymi

            for (int x = 0; x < liczbaObserwacji; x++)
            {

                temp = temp + "<tr><td>"+(okresPoczatkowy+x)+"</td>";

                temp = temp + "<td>"+macierzY[x]+"</td>";



                for (int y = 1; y <= liczbaSzacowanychParametrow-1; y++)
                {
                    temp = temp + String.Format("<td>{0}</td>",macierzX[x,y]);

                }



                temp = temp + "</tr>";
            }

            temp = temp + "</table>";

            szablon = szablon.Replace("[zmienne_dane]", temp);

            temp = "";

            // nazwy zmiennych

            temp = "Zmienna objasniana: " + zmiennaY+"<br />Zmienne objasniajace:<br />";

            for (int x = 0; x < liczbaSzacowanychParametrow-1; x++)
            {
                temp = temp + String.Format("-X{1}: {0} <br />",zmienneX[x],x+1);
            }

            szablon = szablon.Replace("[zmienne_nazwy]", temp);

            // alfy

            temp = "";

            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                temp = temp + String.Format("<tr><td>alfa_{0}:</td><td>{1}</td></tr>",x,alfy[x]);
            }

            szablon = szablon.Replace("[dane_alfy]", temp);


            // Współczynnik determinacji oraz interdeterminacji

            szablon = szablon.Replace("[dana_R2]", wspolczynnikR.ToString());
            szablon = szablon.Replace("[dana_fi2]", wspolczynnikFi.ToString());
            szablon = szablon.Replace("[dana_S2e]", wspolczynnikSKwadratE.ToString());
            szablon = szablon.Replace("[dana_Se]", wspolczynnikSe.ToString());
            szablon = szablon.Replace("[dana_Vse]", wspolczynnikVse.ToString());

            // Badanie istotnosci wspływu zmiennych objaśniających

            // Statystyka D

            temp = "";

            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                temp = temp + "<tr>";
                string interpretacja = String.Format("Int: mowiac, ze ocena parametru alfa_{0} wynosi {1} mylimy się przeciętnie o {2}",x,alfy[x],WektorDAlfTeoretycznych[x]);
                temp = temp + String.Format("<td>D(a[{0}])</td><td>{1}</td><td>{2}</td>", x, WektorDAlfTeoretycznych[x], interpretacja);
                temp = temp + "</tr>";
            }

            

            szablon = szablon.Replace("[dane_wspolczynnikD]", temp);

            // Statystyka t-studenta

            temp = "";
            for (int x = 0; x < liczbaSzacowanychParametrow; x++)
            {
                temp = temp + "<tr><td>";

                if (x == 0) temp = temp + String.Format("alfa[0]<br />(paramter wolny)</td>");
                else temp = temp + String.Format("alfa[{0}]</td>",x);

                if (wektorTstudenta[x] <= 2) temp = temp + String.Format("<td>{0} <= 2</td><td>Parametr nie istotny statystycznie</td>", wektorTstudenta[x]);
                else temp = temp + String.Format("<td>{0} > 2</td><td>Parametr istotny statystycznie</td>", wektorTstudenta[x]);
                
                temp = temp + "</tr>";

            }

            szablon = szablon.Replace("[dane_statystykaTstudenta]", temp);
            temp = "";

            // Badanie autokorelacji składnika losowego

            // Test Durbina-Watsona

            if (wspolczynnikPTDW == false) szablon = szablon.Replace("[dane_h1_TDW]", "p > 0 - wystepuje dodatnia autokorelacja skladnika losowego");
            else szablon = szablon.Replace("[dane_h1_TDW]", "p < 0 - wystepuje ujemna autokorelacja skladnika losowego");

            szablon = szablon.Replace("[wynik_TDW]", wynikTestuDW);
            szablon = szablon.Replace("[int_TDW]", intTestuDW);

            // Test serii

            szablon = szablon.Replace("[wynik_TS]", wynikTestuSerii);
            szablon = szablon.Replace("[int_TS]", intTestuSerii);

            // Test Haryleya

            if (wykonanoTestHartleya == true)
            {
                szablon = szablon.Replace("[wynik_TH]", wynikTestuH);
                szablon = szablon.Replace("[int_TH]", intTestuH);
            }
            else
            {
                szablon = szablon.Replace("[wynik_TH]", "Nie wykonano testu. (za mala ilosc obserwacji)");
                szablon = szablon.Replace("[int_TH]", "");
            }

            // zapis pliku
            dirZapisu = dirZapisu.Replace("/", "");
            //MessageBox.Show(dirZapisu);
            File.WriteAllText(@dirZapisu, szablon, System.Text.ASCIIEncoding.UTF8);

            //System.IO.File.Write(@"test.html", szablon);

        }


        public AkcjeModelu(Form1 form1)
        {
            this.OknoGlowne = form1;
            InitializeComponent();

            Visible = false;
            SprawdzenieZmiennych();
            PrzypisywanieWartosciDoMacierzy();
            KMNK();
            MiaryDopasowania();
            IstotnoscParametrowStrukturalnych();
            BadanieWlasnosciSkladnikaLosowego();

            // przypisywanie danych do wykresu
            comboBox1.Items.Clear();
            comboBox1.Items.Add(zmiennaY);

            foreach (string nazwa in zmienneX) comboBox1.Items.Add(nazwa);

            comboBox1.SelectedIndex = 0;



            try
            {
                Visible = true;
            }
            catch
            {
                Close();
            }
            


        }





        private void button8_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            TworzenieWykresow(comboBox1.GetItemText(comboBox1.SelectedItem));
            //MessageBox.Show("Pokaz wykres");
        }

        private void wykres_Load(object sender, EventArgs e)
        {
            TworzenieWykresow(zmiennaY);
            
            
            //wykres.GraphPane.Title = "dupa";
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TworzenieWykresow(comboBox1.GetItemText(comboBox1.SelectedItem));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // generowanie raportu

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "plik html (*.html)|*.html";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string dir = String.Format("{0}/{1}", saveFileDialog1.InitialDirectory, saveFileDialog1.FileName);
                //MessageBox.Show(dir);
                GenerujRaport(dir);

            }


            
            

        }
    }
}
