# KMNK

Program został przygotowany w ramach zaliczenia przedmiotu na studiach. Napisany został w C# i służy do tworzenia / szacowania modeli ekonometrycznych przy wykorzystaniu danych udostępnianych przez GUS.

Wykorzystuje metodę zwaną Klasyczną Metodą Najmniejszych Kwadratów oraz wykonuje najpopularniejsze testy statystyczne przy tej metodzie dla oszacowania dopasowania modelu względem rzeczywistości.

## Video 
[![Watch the video](readme_files/KMNK.png)](readme_files/KMNK.mp4)
